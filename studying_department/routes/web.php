<?php
use App\Http\Controllers\AdminsController;
use App\Http\Controllers\TeachersController;
use App\Http\Controllers\GroupsController;
use App\Http\Controllers\StudentsController;
use Illuminate\Support\Facades\Route;

Route::get('/', [AdminsController::class, 'login']);
Route::get('/login', [AdminsController::class, 'login'])->name('login');
Route::get('/home', [AdminsController::class, 'home'])->name('home');
Route::post('/api/check', [AdminsController::class, 'check'])->name('check');

Route::get('/teachers', [TeachersController::class, 'for_teachers'])->name('for_teachers');
Route::post('/new_lesson', [TeachersController::class, 'new_lesson'])->name('new_lesson');
Route::post('/save_lesson', [TeachersController::class, 'save_lesson'])->name('save_lesson');

Route::get('/groups', [GroupsController::class, 'groups_editor'])->name('groups_editor');
Route::post('/save_group', [GroupsController::class, 'save_group'])->name('save_group');
Route::post('/delete_group', [GroupsController::class, 'delete_group'])->name('delete_group');
 
Route::post('/students', [StudentsController::class, 'get_students'])->name('get_students');;
Route::get('/student/{student_id}', [StudentsController::class, 'get_student'])->name('get_student');
Route::get('/student/{student_id}/edit', [StudentsController::class, 'edit_student'])->name('edit_student');
Route::get('/student/add/{group_id}', [StudentsController::class, 'add_student'])->name('add_student');
Route::post('/update_student', [StudentsController::class, 'update_student'])->name('update_student');
Route::post('/save_student', [StudentsController::class, 'save_student'])->name('save_student');
Route::post('/delete_student', [StudentsController::class, 'delete_student'])->name('delete_student');