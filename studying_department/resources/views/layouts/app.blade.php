<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="csrf-token" content="{{ csrf_token() }}" />
  <title>Учебный центр | @yield('title')</title>

  <!-- Bootstrap core CSS -->
  <link href="/css/bootstrap.min.css" rel="stylesheet">

  <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
  <script type="text/javascript">
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
  </script>
  
  <style>
    .bd-placeholder-img {
      font-size: 1.125rem;
      text-anchor: middle;
      -webkit-user-select: none;
      -moz-user-select: none;
      -ms-user-select: none;
      user-select: none;
    }

    @media (min-width: 768px) {
      .bd-placeholder-img-lg {
        font-size: 3.5rem;
      }
    }

    footer {
      padding: 20px 0;
      background-color: #dfd7d7;
      width: 100%;
    }

    body {
      display: flex;
      min-height: 100vh;
      flex-direction: column;
    }

    main {
      flex: 1;
    }
  </style>
</head>
<body>

{{-- HEADER --}}
<nav class="navbar navbar-expand-sm navbar-dark bg-dark mb-3">
  <div class="container">
    <a class="navbar-brand" href="{{route('home')}}" onclick="{{Request::is('home') ? 'return false' : ''}}">
      <span class="h3">ОНАХТ</span>
    </a>

    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
  
    <div class="collapse navbar-collapse" id="navbarResponsive">
      <ul class="navbar-nav ml-auto">
        <li class="nav-item  {{Request::is('home') ? 'active' : ''}}">
          <a class="nav-link" href="{{route('home')}}" onclick="{{Request::is('home') ? 'return false' : ''}}">Головна</a>
        </li>

        <li class="nav-item {{Request::is('groups') ? 'active' : ''}}">
          <a class="nav-link" href="{{route('groups_editor')}}" onclick="{{Request::is('groups_editor') ? 'return false' : ''}}">Додати/видалити групу</a>
        </li>

        <li class="nav-item {{Request::is('teachers') ? 'active' : ''}}">
          <a class="nav-link" href="{{route('for_teachers')}}" onclick="{{Request::is('for_teachers') ? 'return false' : ''}}">Для викладачiв </a>
        </li>

        <li class="nav-item ml-2 {{Request::is('/') ? 'active' : ''}}">
          <a class="btn btn-danger" href="/" onclick="{{Request::is('/') ? 'return false' : ''}}" role="button">Вихiд</a>        
        </li>  
      </ul>
    </div>
  </div>
</nav>

{{-- MAIN --}}
<main role="main" class="main-role">
    <div class="container">
        @yield('content')
    </div>
</main>

{{-- FOOTER --}}
<footer>
  <div class="container text-center"> 
    <p class="text-muted">Одеська національна академія харчових технологій</p>
    <p class="text-muted">Заснован 20 жовтня 1902 г.</p>
    <p class="text-muted">65039, г. Одеса, вул.Канатна, 112</p>
    <p class="text-muted">Приймальня ректора: (048) 725-32-84</p>
    <p class="text-muted">e-mail: postmaster@onaft.edu.ua</p>
  </div>
</footer>

<script src="/js/bootstrap.bundle.min.js"></script>
<script type="text/javascript" src="{{URL::asset('js/validation_function.js')}}"></script>
</body>
</html>