@extends('layouts.app')
@section('title', 'Для викладачiв')

@section('content')
<div class="row mb-4">
    <div class="col-12 mb-3">
        <div>
            <p class="h4 text-muted">Нова лекцiя</p> 
        </div>
    </div>
    
    <div class="col-12 mb-3">
        <form method="post" action="{{route('new_lesson')}}">
            @csrf
    
            <div class="form-group">
                <label for="select_group">Група</label>
                <select class="form-control" name="select_group">
                    @for ($i = 0; $i < count($groups); $i++)
                    <option value="{{$groups[$i]->id}}" {{$i == 0 ? 'selected' : ''}}>{{$groups[$i]->name}} ({{$groups[$i]->faculty_name}}, {{$groups[$i]->fod_name}}, {{$groups[$i]->academic_year}} курс)</option>
                    @endfor
                </select>
            </div>

            <div class="form-group">
                <label for="select_subject_and_teacher">Предмет</label>
                <select class="form-control" name="select_subject_and_teacher">
                    @for ($i = 0; $i < count($t_with_s); $i++)
                    <option value="{{$t_with_s[$i]->id}}" {{$i == 0 ? 'selected' : ''}}>{{$t_with_s[$i]->subject_name}} (викладач {{$t_with_s[$i]->teacher_name}})</option>
                    @endfor
                </select>
            </div>

            <input type="submit" name="submit" value="Почати лекцiю" class="btn btn-primary">
        </form> 
    </div>
</div>
@endsection