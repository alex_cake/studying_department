@extends('layouts.app')
@section('title', 'Групи')

@section('content')
<div class="row mb-4">
    <div class="col-12">
        <form class="needs-validation" novalidate method="post" action="{{route('save_group')}}">
            @csrf

            @if ($warning)
                <div class="alert alert-warning" role="alert">
                    Неможливо видалити групу так як в ній є студенти!
                </div>
            @endif

            <div class="form-group">
                <div>
                    <p class="h4 text-muted">Створити нову групу</p> 
                </div>
            </div>
 
            <div class="form-group">
                <label for="name">Назва групи</label>
                <input type="text" class="form-control" name="name" required maxlength="20">
            </div>

            <div class="form-group">
                <label for="select_faculty">Факультет</label>
                <select class="form-control" name="select_faculty" required>
                    @for ($i = 0; $i < count($faculties); $i++)
                        <option value="{{$faculties[$i]->id}}" {{$i == 0 ? 'selected' : ''}}>{{$faculties[$i]->name}}</option>
                    @endfor
                </select>
            </div>

            <div class="form-group">
                <label for="select_fod">Форма навчання</label>
                <select class="form-control" name="select_fod" required>
                    @for ($i = 0; $i < count($fods); $i++)
                        <option value="{{$fods[$i]->id}}" {{$i == 0 ? 'selected' : ''}}>{{$fods[$i]->name}}</option>
                    @endfor
                </select>
            </div>

            <input type="submit" name="submit" value="Зберегти" class="btn btn-primary">
        </form>
    </div>
</div>

<div class="row mb-4">
    <div class="col-12">
        <form class="needs-validation" novalidate method="post" action="{{route('delete_group')}}" data-confirm="Ви впевненi, що хочете видалити групу?">
            @csrf

            <div class="form-group">
                <div>
                    <p class="h4 text-muted">Видалити групу</p> 
                </div>
            </div>

            <div class="form-group">
                <select class="form-control" name="select_group">
                    @for ($i = 0; $i < count($groups); $i++)
                        <option value="{{$groups[$i]->id}}" {{$i == 0 ? 'selected' : ''}}>{{$groups[$i]->name}} ({{$groups[$i]->faculty_name}}, {{$groups[$i]->fod_name}}, {{$groups[$i]->academic_year}} курс)</option>
                    @endfor
                </select>
            </div>

            <input type="submit" name="submit" value="Видалити" class="btn btn-danger">
            <input type="submit" name="submit" value="Видалити з усіма студентами" class="btn btn-danger">
        </form>
    </div>
</div>

<script>
    $(document).ready(function() {
        $(document).on('submit', 'form[data-confirm]', function(e) {
            if(!confirm($(this).data('confirm'))) {
                e.stopImmediatePropagation();
                e.preventDefault();
            }
        });
    });
</script>
@endsection