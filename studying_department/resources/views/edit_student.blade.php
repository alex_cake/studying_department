@extends('layouts.app')
@section('title', 'Студент')

@section('content')
<div class="row mb-5">
    <div class="col-12">
        <p class="h4">Студент {{$student->last_name}} {{$student->first_name}} {{$student->surname}}</p>
    </div>

    <div class="col-12">
        <form class="needs-validation" novalidate method="post" action="{{route('update_student')}}">
            @csrf

            <input type="text" name="id" hidden value="{{$student->id}}">

            <div class="form-group">
                <label for="last_name">Призвище</label>
                <input type="text" class="form-control" name="last_name" value="{{$student->last_name}}" required maxlength="50">
            </div>
            
            <div class="form-group">
                <label for="first_name">Им'я</label>
                <input type="text" class="form-control" name="first_name" value="{{$student->first_name}}" required maxlength="50">
            </div>

            <div class="form-group">
                <label for="surname">Побатьковi</label>
                <input type="text" class="form-control" name="surname" value="{{$student->surname}}" required maxlength="50">
            </div>

            <div class="form-group">
                <label for="birthday">Дата народження</label>
                <input type="date" class="form-control" name="birthday" value="{{$student->birthday}}" required>
            </div>

            <div class="form-group">
                <label class="mr-2">Стать</label>
                @foreach ($genders as $gender)
                <label class="radio-inline mr-2">
                    <input type="radio" name="gender" {{$student->gender_id == $gender->id ? 'checked' : ''}} value="{{$gender->id}}" required> {{$gender->name}}
                </label>
                @endforeach
            </div>

            <div class="form-group">
                <label for="phone">Телефон</label>
                <input type="text" class="form-control" name="phone" value="{{$student->phone}}" required maxlength="20">
            </div>

            <div class="form-group">
                <label for="home_address">Домашня адреса</label>
                <input type="text" class="form-control" name="home_address" value="{{$student->home_address}}" required maxlength="100">
            </div>

            <div class="form-group">
                <label for="select_group">Група</label>

                <select class="form-control" name="select_group" required>
                    @for ($i = 0; $i < count($groups); $i++)
                    <option value="{{$groups[$i]->id}}" {{$student->group_id == $groups[$i]->id ? 'selected' : ''}}>{{$groups[$i]->name}} ({{$groups[$i]->faculty_name}}, {{$groups[$i]->fod_name}}, {{$groups[$i]->academic_year}} курс)</option>
                    @endfor
                </select>

                <div class="invalid-feedback">
                    Please choose a hotel.
                </div>
            </div>

            <div class="form-group">
                <label class="mr-2">Форма оплати:</label>
                @foreach ($payments as $payment)
                <label class="radio-inline mr-2">
                    <input type="radio" name="payment" {{$student->payment_id == $payment->id ? 'checked' : ''}} value="{{$payment->id}}" required> {{$payment->name}}
                </label>
                @endforeach
            </div>

            <div class="form-group {{$student->gender == 'чоловіча' ? '' : 'd-none'}}">
                <label class="mr-2">Військовий квиток:</label>
                
                <label class="radio-inline mr-2">
                    <input type="radio" name="military_accounting" {{$student->military_accounting_TF == 1 ? 'checked' : ''}} value="1"> так
                </label>
                
                <label class="radio-inline mr-2">
                    <input type="radio" name="military_accounting" {{$student->military_accounting_TF == 0 ? 'checked' : ''}} value="0"> нi
                </label>
            </div>

            <div class="form-group">
                <label class="mr-2">Академiчна вiдпустка:</label>

                <label class="radio-inline mr-2">
                    <input type="radio" name="academic_vacation" {{$student->academic_vacation_TF == 1 ? 'checked' : ''}} value="1"> так
                </label>

                <label class="radio-inline mr-2">
                    <input type="radio" name="academic_vacation" {{$student->academic_vacation_TF == 0 ? 'checked' : ''}} value="0"> нi
                </label>
            </div>

            <div class="form-group">
                <label class="mr-2">Вiдрахування:</label>

                <label class="radio-inline mr-2">
                    <input type="radio" name="expelled" {{$student->expelled_TF == 1 ? 'checked' : ''}} value="1"> так
                </label>

                <label class="radio-inline mr-2">
                    <input type="radio" name="expelled" {{$student->expelled_TF == 0 ? 'checked' : ''}} value="0"> нi
                </label>
            </div>

            <div class="form-group">
                <label for="documents">Документи</label>
                <textarea class="form-control" name="documents" value="{{$student->documents}}" rows="3" maxlength="500"></textarea>
            </div>

            <input type="submit" name="submit" value="Зребегти" class="btn btn-primary">
        </form> 
    </div>
</div>
@endsection