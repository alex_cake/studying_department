@extends('layouts.app')
@section('title', 'Студент')

@section('content')

<div class="row mb-3">
    <div class="col-12">
        <p class="h4">Студент {{$student->last_name}} {{$student->first_name}} {{$student->surname}}</p>
    </div>
</div>

<div class="row mb-4">
    <div class="col-12 col-sm-6 mb-4">
        <div class="card mb-4">
            <div class="card-header">
                <h2 class="h5">Особиста iнформацiя</h2>
            </div>

            <ul class="list-group list-group-flush">
                <li class="list-group-item">Стать: {{$student->gender}}</li>
                <li class="list-group-item">Дата народження: {{$student->birthday}}</li>
                <li class="list-group-item">Телефон: {{$student->phone}}</li>
                <li class="list-group-item">Домашня адреса: {{$student->home_address}}</li>
                <li class="list-group-item">Документи: {{$student->documents}}</li>
            </ul>
        </div>
        
        <div>
            <a class="btn btn-primary mb-2" href="{{route('edit_student', ['student_id' => $student->id])}}" role="button">Редагувати</a>
        </div>

        <form method="post" action="{{route('delete_student')}}" data-confirm="Ви впевненi, що хочете видалити студента?">
            @csrf
            <input type="text" name="student_id" hidden value="{{$student->id}}">

            <input type="submit" name="submit" value="Видалити" class="btn btn-danger">
        </form>
    </div>

    <div class="col-12 col-sm-6 mb-4">
        <div class="card">
            <div class="card-header">
                <h3 class="h5">Основна iнформацiя</h3>
            </div>

            <ul class="list-group list-group-flush">
                <li class="list-group-item">Група: {{$student->group_number}}</li>
                <li class="list-group-item">Факультет: {{$student->faculty}}</li>
                <li class="list-group-item">Курс: {{$student->academic_year}}</li>
                <li class="list-group-item">Форма навчання: {{$student->fod}}</li>
                <li class="list-group-item">Форма оплати: {{$student->payment}}</li>
                
                @if ($student->gender == 'чоловіча')
                    <li class="list-group-item">Військовий квиток: {{$student->military_accounting_TF == 1 ? 'так' : 'нi'}}</li>
                @endif

                <li class="list-group-item">Академiчна вiдпустка: {{$student->academic_vacation_TF == 1 ? 'так' : 'нi'}}</li>
                <li class="list-group-item">Вiдрахування: {{$student->expelled_TF == 1 ? 'так' : 'нi'}}</li>
                
                @if ($student->payment == 'Контракт')
                    <li class="list-group-item {{$payment_warning_class}}">Наступна оплата: {{$student->next_payment}}</li>
                @endif
            </ul>
        </div>
    </div>

    @if ($presence_warning == 1)
        <div class="col-12 mb-4">
            <div class="alert alert-danger" role="alert">
                У студента забагато пропускiв!
            </div>
        </div>
    @endif

    <div class="col-12 mb-4">
        <div class="card">
            <div class="card-header">
                <h3 class="h5">Оцiнки</h3>
            </div>

            @foreach ($grades as $key => $grade)
                <ul class="list-group list-group-flush">
                    <li class="list-group-item list-group-item-info">Предмет {{$grade->get(0)->subject_name}} (вичкладач {{$grade->get(0)->teacher_name}})</li>
                </ul>
                
                <div class="ml-4">
                    @for ($i = 0; $i < count($grade); $i++)
                        <p>
                            <span>{{$grade->get($i)->presence == '1' ? 'Був присутнiй' : ($grade->get($i)->presence == '2' ? 'Спiзнився' : 'Був вiдсутнiй')}}. </span>
                            
                            <span>Лекцiя {{$grade->get($i)->lesson_number}} вiд {{$grade->get($i)->date}}.</span>

                            @if ($grade->get($i)->grade != 0)
                                <span>Оцiнка - {{$grade->get($i)->grade}} - {{$grade->get($i)->description}}. </span>
                            @endif
                        </p>
                    @endfor
                    
                    <p>Усього: {{$grade->where('grade', '>', 0)->pluck('grade')->sum()}}</p>
                </div>
            @endforeach
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        $(document).on('submit', 'form[data-confirm]', function(e) {
            if(!confirm($(this).data('confirm'))) {
                e.stopImmediatePropagation();
                e.preventDefault();
            }
        });
    });
</script>
@endsection