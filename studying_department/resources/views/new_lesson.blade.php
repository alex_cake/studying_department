@extends('layouts.app')
@section('title', 'Лекцiя')

@section('content')
<style>
    .custom-control-input:checked~.custom-control-label::before {
        color: #fff;
        border-color: #7B1FA2;
    }

    .custom-control-input:checked~.custom-control-label.red::before {
        background-color: red;
    }

    .custom-control-input:checked~.custom-control-label.yellow::before {
        background-color: yellow;
    }

    .custom-control-input:checked~.custom-control-label.green::before {
        background-color: green;
    }
</style>

<div class="row mb-4">
    <div class="col-12 mb-3">
        <p class="h5">Предмет {{$teacher_with_subject->subject_name}} (вичкладач {{$teacher_with_subject->teacher_name}})</p>
        <p class="h5">Група {{$group->name}} ({{$group->faculty_name}}, {{$group->fod_name}}, {{$group->academic_year}} курс)</p>
    </div>

    <div class="col-12 mb-3">
        <form method="post" action="{{route('save_lesson')}}">
            @csrf
            <input type="text" name="teacher_with_subject_id" hidden value="{{$teacher_with_subject->id}}">
            <input type="text" name="group_id" hidden value="{{$group->id}}">

            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <label class="input-group-text" for="select_lesson_number">Пара №</label>
                </div>

                <select class="custom-select" name="select_lesson_number">
                    @for ($i = 1; $i <= 9; $i++)
                        <option value="{{$i}}" {{$i == 1 ? 'selected' : ''}}>{{$i}}</option>
                    @endfor
                </select>
            </div>

            <table class="table table-striped table-sm">
                <thead>
                    <tr>
                        <th scope="col">№</th>
                        <th scope="col-4">Студент</th>
                        <th scope="col-2">Присутнiсть</th>
                        <th scope="col-2">Оцiнка</th>
                        <th scope="col-3">Опис</th>
                    </tr>
                </thead>
                
                <tbody>
                    @for ($i = 0; $i < count($students); $i++)
                        <tr>
                            <th scope="row">{{$i + 1}}</th>

                            <td>
                                {{$students[$i]->name}}
                            </td>

                            <td>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="{{$students[$i]->id}}r1" name="presence_{{$students[$i]->id}}" class="custom-control-input" value="1" checked>
                                    <label class="custom-control-label green" for="{{$students[$i]->id}}r1"></label>
                                </div>

                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="{{$students[$i]->id}}r2" name="presence_{{$students[$i]->id}}" class="custom-control-input" value="2" >
                                    <label class="custom-control-label yellow" for="{{$students[$i]->id}}r2"></label>
                                </div>

                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="{{$students[$i]->id}}r3" name="presence_{{$students[$i]->id}}" class="custom-control-input" value="0">
                                    <label class="custom-control-label red" for="{{$students[$i]->id}}r3"></label>
                                </div>    
                            </td>
                                
                            <td>
                                <input type="number" class="form-control" name="grade_{{$students[$i]->id}}" min="1" max="100" step="1">
                            </td>

                            <td>
                                <input type="text" class="form-control" name="description_{{$students[$i]->id}}" value="Робота на уроцi" maxlength="50">
                            </td>
                        </tr>
                    @endfor
                </tbody>
            </table>

            <input type="submit" name="submit" value="Зребегти" class="btn btn-primary">

        </form> 
    </div>
</div>

<script>
    $(document).ready(function() {
        $("#select_group").change(function(event){
            event.preventDefault();
                
            $.ajax({
                url: "/students",
                type:"POST",
                data:{ id:$("#select_group").val()},
                success:function(response) {
                    $("#div_students").removeClass("d-none");
                    $("#students_links").html(response);
                },
            });
        });
    });
</script>
@endsection