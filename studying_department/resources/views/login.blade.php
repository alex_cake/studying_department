@extends('layouts.app')
@section('title', 'Вхiд')

@section('content')
<form class="needs-validation mb-4" novalidate method="post" action="{{route('check')}}">
    @csrf

    <div class="form-row">
        <div class="col-12 col-sm-6 mb-3">
            <label for="login">Login</label>
            <input type="text" class="form-control" name="login" placeholder="Enter login..." required minlength="5" maxlength="20">
        </div>

        <div class="col-12 col-sm-6 mb-3">
            <label for="password">Password</label>
            <input type="password" class="form-control" name="password" required minlength="8" maxlength="20">
        </div>
    </div>
    
    @if ($alert == 1)
    <div class="alert alert-danger" id="alert">
        Wrong login or password! Try again.
    </div>
    @endif

    <button id="submit" class="btn btn-primary">Submit</button>
</form>
@endsection