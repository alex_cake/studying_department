@extends('layouts.app')
@section('title', 'Головна')

@section('content')
<div class="row mb-4">
    <div class="col-12 mb-3">
        <div>
            <p class="h4 text-muted">Iнформацiя про студентiв</p> 
        </div>
    </div>

    <div class="col-12 mb-3">
        <label for="select_group">Група</label>
        <select class="form-control" id="select_group">
            <option style="display: none">Виберiть групу...</option>
            @foreach ($groups as $group)
                <option value="{{$group->id}}">{{$group->name}} ({{$group->faculty_name}}, {{$group->fod_name}}, {{$group->academic_year}} курс)</option>
            @endforeach
        </select>
    </div>

    <div id="div_students" class="col-12 mb-3 d-none">       
        <div id="students_links">
        </div>
    </div>
</div>

<div class="row mb-4">
    <div class="col-12">
        {!! $warnings !!}
    </div>
</div>
<script>
    $(document).ready(function() {
        $("#select_group").change(function(event){
            event.preventDefault();
                
            $.ajax({
                url: "/students",
                type:"POST",
                data:{ id:$("#select_group").val()},
                success:function(response) {
                    $("#div_students").removeClass("d-none");
                    $("#students_links").html(response);
                },
            });
        });
    });
</script>
@endsection