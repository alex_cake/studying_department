@extends('layouts.app')
@section('title', 'Cтудент')

@section('content')
<div class="row mb-5">
    <div class="col-12">
    <p class="h5 text-muted">Новий студент у групу {{$group->name}} ({{$group->faculty_name}}, {{$group->fod_name}}, {{$group->academic_year}} курс)</p>
    </div>

    <div class="col-12">
        <form class="needs-validation" novalidate method="post" action="{{route('save_student')}}">
            @csrf
            <input type="text" name="group_id" hidden value="{{$group->id}}">

            <div class="form-group">
                <label for="last_name">Призвище</label>
                <input type="text" class="form-control" name="last_name" required maxlength="50">
            </div>
            
            <div class="form-group">
                <label for="first_name">Им'я</label>
                <input type="text" class="form-control" name="first_name" required maxlength="50">
            </div>

            <div class="form-group">
                <label for="surname">Побатьковi</label>
                <input type="text" class="form-control" name="surname" required maxlength="50">
            </div>

            <div class="form-group">
                <label for="birthday">Дата народження</label>
                <input type="date" class="form-control" name="birthday" required>
            </div>

            <div class="form-group">
                <label class="mr-2">Стать</label>
                
                @for ($i = 0; $i < count($genders); $i++)
                    <label class="radio-inline mr-2">
                        <input type="radio" name="gender" {{$i == 0 ? 'checked' : ''}} value="{{$genders[$i]->id}}"> {{$genders[$i]->name}}
                    </label>
                @endfor
            </div>

            <div class="form-group">
                <label for="phone">Телефон</label>
                <input type="text" class="form-control" name="phone" required maxlength="20">
            </div>

            <div class="form-group">
                <label for="home_address">Домашня адреса</label>
                <input type="text" class="form-control" name="home_address" required maxlength="100">
            </div>

            <div class="form-group">
                <label class="mr-2">Форма оплати:</label>
                
                @for ($i = 0; $i < count($payments); $i++)
                    <label class="radio-inline mr-2">
                        <input type="radio" name="payment" {{$i == 0 ? 'checked' : ''}} value="{{$payments[$i]->id}}"> {{$payments[$i]->name}}
                    </label>
                @endfor
            </div>

            <div class="form-group">
                <label class="mr-2">Військовий квиток:</label>

                <label class="radio-inline mr-2">
                    <input type="radio" name="military_accounting" checked value="1"> так
                </label>

                <label class="radio-inline mr-2">
                    <input type="radio" name="military_accounting" value="0"> нi
                </label>
            </div>

            <div class="form-group">
                <label class="mr-2">Академiчна вiдпустка:</label>

                <label class="radio-inline mr-2">
                    <input type="radio" name="academic_vacation" value="1">так
                </label>

                <label class="radio-inline mr-2">
                    <input type="radio" name="academic_vacation" checked value="0">нi
                </label>
            </div>

            <div class="form-group">
                <label class="mr-2">Вiдрахування:</label>

                <label class="radio-inline mr-2">
                    <input type="radio" name="expelled" value="1">так
                </label>

                <label class="radio-inline mr-2">
                    <input type="radio" name="expelled" checked value="0">нi
                </label>
            </div>

            <div class="form-group">
                <label for="documents">Документи</label>
                <textarea class="form-control" name="documents" rows="3" maxlength="500"></textarea>
            </div>

            <input type="submit" name="submit" value="Зребегти" class="btn btn-primary">
        </form> 
    </div>
</div>
@endsection