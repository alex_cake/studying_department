<?php
namespace App\Http\Controllers;
use App\Models\Admin;
use App\Models\Grade;
use App\Models\Group;
use App\Models\Student;
use DateInterval;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AdminsController extends Controller
{
    public function login()
    {
        return view('login', ['alert' => 0]);
    }

    public function check(Request $request)
    {
        $ph = Admin::where('login', $request->login)
        ->where('password_hash', hash('sha256', $request->password))
        ->get();

        return $ph != null && count($ph) == 1 ? 
            redirect()->route('home') : 
            redirect()->route('login', ['alert' => 1]); 
    }

    public function home()
    {
        $groups = Group::join('faculties', 'faculties.id', '=', 'groups.faculty_id')
        ->join('forms_of_education as fod', 'fod.id', '=', 'groups.form_of_education_id')
        ->select('groups.id', 'groups.name', 'faculties.name as faculty_name', 'fod.name as fod_name', 'academic_year')
        ->get();

        $students = Student::select('id', 'next_payment', DB::raw('CONCAT(last_name, " ", first_name, " ", surname) as name'), 'payment_id')
        ->get();

        $warnings = '';
        foreach ($students as $student) {
            // Количество присутствий
            $presence_yes = Grade::where('student_id', $student->id)
            ->where('presence', '!=', '0')
            ->count();
            // Количество отсутствий
            $presence_not = Grade::where('student_id', $student->id)
            ->where('presence', '0')
            ->count();
            
            if ($presence_yes < $presence_not) {
                $warnings .= '<div class="alert alert-danger" role="alert">Студент <a href="' 
                . route('get_student', ['student_id' => $student->id]) 
                . '" class="alert-link">' 
                . $student->name 
                . '</a> має занадто багато пропусків!</div>';
            }
            
            // Просроченная оплата
            if ((new DateTime("now"))->add(new DateInterval('P1M')) >= new DateTime($student->next_payment) && $student->payment_id == '2') {
                $warnings .= '<div class="alert alert-danger" role="alert">Студент <a href="' 
                . route('get_student', ['student_id' => $student->id]) 
                . '" class="alert-link">' 
                . $student->name 
                . '</a> має скоро оплатити навчання!</div>';
            }
        }
        
        return view('home', ['groups' => $groups, 'warnings' => $warnings]);
    }
}