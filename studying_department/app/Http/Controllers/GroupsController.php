<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Group;
use App\Models\Student;
use Exception;

class GroupsController extends Controller
{
    public function groups_editor($warning = false)
    {
        $faculties = DB::table('faculties')->select('id', 'name')->get();
        $fods = DB::table('forms_of_education')->select('id', 'name')->get();
        $groups = Group::join('faculties', 'faculties.id', '=', 'groups.faculty_id')
        ->join('forms_of_education as fod', 'fod.id', '=', 'groups.form_of_education_id')
        ->select('groups.id', 'groups.name', 'faculties.name as faculty_name', 'fod.name as fod_name', 'academic_year')
        ->get();


        return view('groups_editor', ['groups' => $groups, 'faculties' => $faculties, 'fods' => $fods, 'warning' => $warning]);
    }

    public function save_group(Request $request)
    {
        Group::insert([
            'name' => $request->name,
            'faculty_id' => $request->select_faculty,
            'form_of_education_id' => $request->input('select_fod')
        ]);

        return redirect()->route('home');
    }

    public function delete_group(Request $request)
    {
        try 
        {
            if ($request->submit == 'Видалити з усіма студентами') {
                Student::where('group_id', $request->select_group)
                ->delete();
            }

            Group::where('id', $request->select_group)
            ->delete();
        
            return redirect()->route('groups_editor');
        } catch (Exception $e) {
            return $this->groups_editor(true);
        }
    }
}