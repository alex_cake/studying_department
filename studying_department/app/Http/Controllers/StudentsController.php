<?php
namespace App\Http\Controllers;
use App\Models\Grade;
use App\Models\Group;
use App\Models\Student;
use DateInterval;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class StudentsController extends Controller
{
    public function get_students(Request $group)
    {
        $students = Student::select('id', DB::raw('CONCAT(last_name, " ", first_name, " ", surname) as name'))
        ->where('group_id', $group->id)
        ->get();

        $response = '';

        if ($students->count() > 0) {
            
            foreach ($students as $student) {
                $response .= '<a href="' 
                    . route('get_student', ['student_id' => $student->id]) 
                    .'" class="list-group-item list-group-item-action">' 
                    . $student->name 
                    . '</a>';
            }
        }
        else {
            $response .= '<p>Група пуста</p>';
        }
        
        $response .= '<a class="btn btn-primary my-3" href="' . route('add_student', ['group_id' => $group->id]) . '" role="button">Додати студента</a>';

        return $response;
    }

    public function get_student($student_id)
    {
        $student = DB::table('studentsview')
        ->select('*')
        ->where('id', $student_id)
        ->first();

        $grades = Grade::join('teachers_with_subjects', 'teachers_with_subjects.id', '=', 'teacher_and_subject_id')
        ->where('student_id', $student_id)
        ->select('*')
        ->get()
        ->groupBy('teacher_and_subject_id');

        // Количество присутствий
        $presence_yes = Grade::where('student_id', $student_id)
        ->where('presence', '!=', '0')
        ->count();
        // Количество отсутствий
        $presence_not = Grade::where('student_id', $student_id)
        ->where('presence', '0')
        ->count();

        // Просроченная оплата
        $today = (new DateTime("now"))->add(new DateInterval('P1M'));
        $target = new DateTime($student->next_payment);

        // presence_warning: 1 - уведомление о пропусках есть, 0 - уведомления нет
        // 
        // payment_warning_class: 
        // list-group-item-danger - класс для уведомления о просроченной оплате будет добавлен,
        // '' - класса для уведомления не будет
        return view('student', ['student' => $student, 'grades' => $grades, 'presence_warning' =>  $presence_yes < $presence_not ? 1 : 0,
        'payment_warning_class' => $today >= $target ? 'list-group-item-danger' : '']);
    }

    public function edit_student($student_id)
    {
        $student = DB::table('studentsview')
        ->select('*')
        ->where('id', $student_id)
        ->first();

        $groups = Group::join('faculties', 'faculties.id', '=', 'groups.faculty_id')
        ->join('forms_of_education as fod', 'fod.id', '=', 'groups.form_of_education_id')
        ->select('groups.id', 'groups.name', 'faculties.name as faculty_name', 'fod.name as fod_name', 'academic_year')
        ->get();

        $genders = DB::table('genders')->select('id', 'name')->get();
        $payments = DB::table('payments')->select('id', 'name')->get();

        return view('edit_student', ['student' => $student, 'groups' => $groups, 'genders' => $genders, 'payments' => $payments]);
    }

    public function add_student($group_id)
    {
        $genders = DB::table('genders')->select('id', 'name')->get();
        $payments = DB::table('payments')->select('id', 'name')->get();
        $group = Group::join('faculties', 'faculties.id', '=', 'groups.faculty_id')
        ->join('forms_of_education as fod', 'fod.id', '=', 'groups.form_of_education_id')
        ->where('groups.id', $group_id)
        ->select('groups.id', 'groups.name', 'faculties.name as faculty_name', 'fod.name as fod_name', 'academic_year')
        ->first();

        return view('add_student', ['genders' => $genders, 'payments' => $payments, 'group' => $group]);
    }

    public function update_student(Request $request)
    {
        $date = getDate(strtotime($request->birthday));

        Student::where('id', $request->id)
        ->update([
            'last_name' => $request->last_name,
            'first_name' => $request->first_name,
            'surname' => $request->surname,
            'birthday' => $date['year'] . '-' .  $date['mon'] . '-' .  $date['mday'],
            'gender_id' => $request->gender,
            'phone' => $request->phone,
            'home_address' => $request->home_address,
            'group_id' => $request->select_group,
            'payment_id' => $request->payment,
            'military_accounting_TF' => $request->military_accounting,
            'academic_vacation_TF' => $request->academic_vacation,
            'expelled_TF' => $request->expelled,
            'documents' => ($request->documents == null ? '' : $request->documents) 
        ]);

        return redirect()->route('get_student', ['student_id' => $request->id]);
    }

    public function save_student(Request $request)
    {
        $date = getDate(strtotime($request->birthday));

        $student_id = Student::insertGetId([
            'last_name' => $request->last_name,
            'first_name' => $request->first_name,
            'surname' => $request->surname,
            'birthday' => $date['year'] . '-' .  $date['mon'] . '-' .  $date['mday'],
            'gender_id' => $request->gender,
            'phone' => $request->phone,
            'home_address' => $request->home_address,
            'group_id' => $request->group_id,
            'payment_id' => $request->payment,
            'military_accounting_TF' => $request->military_accounting,
            'academic_vacation_TF' => $request->academic_vacation,
            'expelled_TF' => $request->expelled,
            'documents' => ($request->documents == null ? '' : $request->documents) 
        ]);
        
        return redirect()->route('get_student', ['student_id' => $student_id]);
    }

    public function delete_student(Request $request)
    {
        Student::where('id', $request->student_id)->delete();

        return redirect()->route('home');
    }
}