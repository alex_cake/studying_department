<?php
namespace App\Http\Controllers;
use App\Models\Group;
use App\Models\Student;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TeachersController extends Controller
{
    public function for_teachers()
    {
        $teacher_with_subject = DB::table('teachers_with_subjects')
        ->select('*')
        ->get();

        $groups = Group::join('faculties', 'faculties.id', '=', 'groups.faculty_id')
        ->join('forms_of_education as fod', 'fod.id', '=', 'groups.form_of_education_id')
        ->select('groups.id', 'groups.name', 'faculties.name as faculty_name', 'fod.name as fod_name', 'academic_year')
        ->get();

        return view('for_teachers', ['t_with_s' => $teacher_with_subject, 'groups' => $groups]);
    }
    
    public function new_lesson(Request $request)
    {
        $students = Student::select('id', DB::raw('CONCAT(last_name, " ", first_name, " ", surname) as name'))
        ->where('group_id', $request->select_group)
        ->get();

        $teacher_with_subject = DB::table('teachers_with_subjects')
        ->where('id', $request->select_subject_and_teacher)
        ->select('*')
        ->first();

        $group = Group::join('faculties', 'faculties.id', '=', 'groups.faculty_id')
        ->join('forms_of_education as fod', 'fod.id', '=', 'groups.form_of_education_id')
        ->where('groups.id', $request->select_group)
        ->select('groups.id', 'groups.name', 'faculties.name as faculty_name', 'fod.name as fod_name', 'academic_year')
        ->first();

        return view('new_lesson', ['students' => $students, 'group' => $group, 'teacher_with_subject' => $teacher_with_subject]);
    }

    public function save_lesson(Request $request)
    {
        $ids = Student::where('group_id', $request->group_id)
        ->pluck('id');

        for ($i = 0; $i < count($ids); $i++) { 
            $presence = $request->input('presence_' . $ids[$i]);
            $grade = $request->input('grade_' . $ids[$i]); 
            $description = $request->input('description_' . $ids[$i]);
            
            DB::table('grades')->insert([
                'student_id' => $ids[$i],
                'teacher_and_subject_id' => $request->teacher_with_subject_id,
                'grade' =>  $grade ? $grade : 0,
                'description' =>  $grade ? $description : '',
                'presence' => $presence,
                'lesson_number' => $request->select_lesson_number
            ]);
        }

        return redirect()->route('for_teachers');
    }
}