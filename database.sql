-- --------------------------------------------------------
-- Хост:                         127.0.0.1
-- Версия сервера:               10.4.14-MariaDB - mariadb.org binary distribution
-- Операционная система:         Win64
-- HeidiSQL Версия:              11.1.0.6116
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Дамп структуры базы данных studying_department
CREATE DATABASE IF NOT EXISTS `studying_department` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
USE `studying_department`;

-- Дамп структуры для таблица studying_department.admins
CREATE TABLE IF NOT EXISTS `admins` (
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `login` varchar(20) NOT NULL,
  `password_hash` varchar(64) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4;

-- Дамп данных таблицы studying_department.admins: ~2 rows (приблизительно)
DELETE FROM `admins`;
/*!40000 ALTER TABLE `admins` DISABLE KEYS */;
INSERT INTO `admins` (`id`, `login`, `password_hash`) VALUES
	(5, 'dekan', '0242c0436daa4c241ca8a793764b7dfb50c223121bb844cf49be670a3af4dd18'),
	(6, 'sergienko', 'ef797c8118f02dfb649607dd5d3f8c7623048c9c063d532cc95c5ed7a898a64f');
/*!40000 ALTER TABLE `admins` ENABLE KEYS */;

-- Дамп структуры для таблица studying_department.faculties
CREATE TABLE IF NOT EXISTS `faculties` (
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

-- Дамп данных таблицы studying_department.faculties: ~2 rows (приблизительно)
DELETE FROM `faculties`;
/*!40000 ALTER TABLE `faculties` DISABLE KEYS */;
INSERT INTO `faculties` (`id`, `name`) VALUES
	(1, 'Автоматизація'),
	(2, 'Робототехніка'),
	(3, 'Кібернетіка');
/*!40000 ALTER TABLE `faculties` ENABLE KEYS */;

-- Дамп структуры для таблица studying_department.forms_of_education
CREATE TABLE IF NOT EXISTS `forms_of_education` (
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

-- Дамп данных таблицы studying_department.forms_of_education: ~3 rows (приблизительно)
DELETE FROM `forms_of_education`;
/*!40000 ALTER TABLE `forms_of_education` DISABLE KEYS */;
INSERT INTO `forms_of_education` (`id`, `name`) VALUES
	(1, 'Стаціонар (денна)'),
	(2, 'Стаціонар (вечірня)'),
	(3, 'Заочна');
/*!40000 ALTER TABLE `forms_of_education` ENABLE KEYS */;

-- Дамп структуры для таблица studying_department.genders
CREATE TABLE IF NOT EXISTS `genders` (
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

-- Дамп данных таблицы studying_department.genders: ~2 rows (приблизительно)
DELETE FROM `genders`;
/*!40000 ALTER TABLE `genders` DISABLE KEYS */;
INSERT INTO `genders` (`id`, `name`) VALUES
	(1, 'чоловіча'),
	(2, 'жіноча');
/*!40000 ALTER TABLE `genders` ENABLE KEYS */;

-- Дамп структуры для таблица studying_department.grades
CREATE TABLE IF NOT EXISTS `grades` (
  `grade_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `student_id` smallint(5) unsigned NOT NULL DEFAULT 0,
  `teacher_and_subject_id` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `presence` tinyint(1) unsigned NOT NULL,
  `grade` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `description` varchar(50) NOT NULL DEFAULT '',
  `lesson_number` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `date` date NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`grade_id`) USING BTREE,
  KEY `FK_grades_subject_teacher` (`teacher_and_subject_id`),
  KEY `student_fk` (`student_id`),
  CONSTRAINT `FK_grades_subject_teacher` FOREIGN KEY (`teacher_and_subject_id`) REFERENCES `subject_teacher` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `student_fk` FOREIGN KEY (`student_id`) REFERENCES `students` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COMMENT='presence принимает значения 0 - отсутствовал, 1 - присутствовал, 2 - присутствовал, но опоздал.';

-- Дамп данных таблицы studying_department.grades: ~0 rows (приблизительно)
DELETE FROM `grades`;
/*!40000 ALTER TABLE `grades` DISABLE KEYS */;
INSERT INTO `grades` (`grade_id`, `student_id`, `teacher_and_subject_id`, `presence`, `grade`, `description`, `lesson_number`, `date`) VALUES
	(1, 4, 1, 1, 5, 'Робота на уроцi', 1, '2020-11-22'),
	(2, 6, 1, 1, 0, '', 1, '2020-11-22'),
	(3, 7, 1, 1, 0, '', 1, '2020-11-22'),
	(4, 4, 1, 1, 8, 'Робота на уроцi', 2, '2020-11-22'),
	(5, 6, 1, 1, 0, '', 2, '2020-11-22'),
	(6, 7, 1, 1, 0, '', 2, '2020-11-22'),
	(7, 4, 1, 1, 10, 'Робота на уроцi', 3, '2020-11-22'),
	(8, 6, 1, 1, 0, '', 3, '2020-11-22'),
	(9, 7, 1, 1, 0, '', 3, '2020-11-22'),
	(10, 4, 1, 0, 0, '', 1, '2020-11-22'),
	(11, 6, 1, 1, 0, '', 1, '2020-11-22'),
	(12, 7, 1, 1, 0, '', 1, '2020-11-22'),
	(13, 4, 1, 0, 0, '', 1, '2020-11-22'),
	(14, 6, 1, 1, 0, '', 1, '2020-11-22'),
	(15, 7, 1, 1, 0, '', 1, '2020-11-22'),
	(16, 4, 1, 0, 0, '', 1, '2020-11-22'),
	(17, 6, 1, 1, 0, '', 1, '2020-11-22'),
	(18, 7, 1, 1, 0, '', 1, '2020-11-22');
/*!40000 ALTER TABLE `grades` ENABLE KEYS */;

-- Дамп структуры для таблица studying_department.groups
CREATE TABLE IF NOT EXISTS `groups` (
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL DEFAULT '0',
  `faculty_id` tinyint(3) unsigned NOT NULL,
  `academic_year` tinyint(3) unsigned NOT NULL DEFAULT 1,
  `form_of_education_id` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `faculty_fk` (`faculty_id`),
  KEY `fod_fk` (`form_of_education_id`),
  CONSTRAINT `faculty_fk` FOREIGN KEY (`faculty_id`) REFERENCES `faculties` (`id`),
  CONSTRAINT `fod_fk` FOREIGN KEY (`form_of_education_id`) REFERENCES `forms_of_education` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4;

-- Дамп данных таблицы studying_department.groups: ~4 rows (приблизительно)
DELETE FROM `groups`;
/*!40000 ALTER TABLE `groups` DISABLE KEYS */;
INSERT INTO `groups` (`id`, `name`, `faculty_id`, `academic_year`, `form_of_education_id`) VALUES
	(3, 'АМ-1', 1, 1, 1),
	(4, 'РТ-1', 2, 2, 2);
/*!40000 ALTER TABLE `groups` ENABLE KEYS */;

-- Дамп структуры для таблица studying_department.payments
CREATE TABLE IF NOT EXISTS `payments` (
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

-- Дамп данных таблицы studying_department.payments: ~2 rows (приблизительно)
DELETE FROM `payments`;
/*!40000 ALTER TABLE `payments` DISABLE KEYS */;
INSERT INTO `payments` (`id`, `name`) VALUES
	(1, 'Бюджет'),
	(2, 'Контракт');
/*!40000 ALTER TABLE `payments` ENABLE KEYS */;

-- Дамп структуры для таблица studying_department.students
CREATE TABLE IF NOT EXISTS `students` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `last_name` varchar(50) NOT NULL DEFAULT '0',
  `first_name` varchar(50) NOT NULL DEFAULT '0',
  `surname` varchar(50) NOT NULL DEFAULT '0',
  `birthday` date NOT NULL,
  `gender_id` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `phone` varchar(20) NOT NULL,
  `home_address` varchar(100) NOT NULL,
  `group_id` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `payment_id` tinyint(3) unsigned NOT NULL,
  `military_accounting_TF` tinyint(1) unsigned NOT NULL DEFAULT 1,
  `academic_vacation_TF` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `expelled_TF` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `documents` varchar(500) DEFAULT '',
  `next_payment` date DEFAULT '2020-12-01',
  PRIMARY KEY (`id`),
  KEY `gender_fk` (`gender_id`),
  KEY `group_fk` (`group_id`),
  KEY `payment_fk` (`payment_id`),
  CONSTRAINT `gender_fk` FOREIGN KEY (`gender_id`) REFERENCES `genders` (`id`),
  CONSTRAINT `group_fk` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`),
  CONSTRAINT `payment_fk` FOREIGN KEY (`payment_id`) REFERENCES `payments` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4;

-- Дамп данных таблицы studying_department.students: ~8 rows (приблизительно)
DELETE FROM `students`;
/*!40000 ALTER TABLE `students` DISABLE KEYS */;
INSERT INTO `students` (`id`, `last_name`, `first_name`, `surname`, `birthday`, `gender_id`, `phone`, `home_address`, `group_id`, `payment_id`, `military_accounting_TF`, `academic_vacation_TF`, `expelled_TF`, `documents`, `next_payment`) VALUES
	(4, 'Маями', 'Олег', 'Евгнатович', '2002-11-06', 1, '88005553535', 'Пушкінська 12/2', 3, 1, 1, 0, 0, '', '2020-12-01'),
	(6, 'Тумбуркат', 'Юрій', 'Федорович', '1997-10-09', 1, '21324324', 'Заболотного 3/4', 3, 2, 1, 0, 0, '', '2020-12-01'),
	(7, 'Созанська', 'Анастасія', 'Володимірівна', '2001-03-22', 2, '12344234234', 'Нижня 23/4', 3, 1, 1, 0, 0, '', '2020-12-01'),
	(8, 'Гаджиев', 'Александр', 'Ахмедович', '2020-01-06', 1, '1234123123', 'Парамона 123/5', 4, 1, 1, 0, 0, '', '2020-12-01'),
	(9, 'Романюк', 'Олена', 'Олегівна', '2002-11-09', 2, '3243242343', 'Тираспольська 34', 4, 2, 1, 0, 0, ' ', '2020-12-01'),
	(10, 'Паріжська', 'Анастасія', 'Степанівна', '1993-03-06', 2, '1234234234', 'Григорія 123/4', 4, 1, 1, 0, 0, '', '2020-12-01');
/*!40000 ALTER TABLE `students` ENABLE KEYS */;

-- Дамп структуры для представление studying_department.studentsview
-- Создание временной таблицы для обработки ошибок зависимостей представлений
CREATE TABLE `studentsview` (
	`id` SMALLINT(5) UNSIGNED NOT NULL,
	`last_name` VARCHAR(50) NOT NULL COLLATE 'utf8mb4_general_ci',
	`first_name` VARCHAR(50) NOT NULL COLLATE 'utf8mb4_general_ci',
	`surname` VARCHAR(50) NOT NULL COLLATE 'utf8mb4_general_ci',
	`birthday` DATE NOT NULL,
	`phone` VARCHAR(20) NOT NULL COLLATE 'utf8mb4_general_ci',
	`home_address` VARCHAR(100) NOT NULL COLLATE 'utf8mb4_general_ci',
	`next_payment` DATE NULL,
	`military_accounting_TF` TINYINT(1) UNSIGNED NOT NULL,
	`academic_vacation_TF` TINYINT(1) UNSIGNED NOT NULL,
	`expelled_TF` TINYINT(1) UNSIGNED NOT NULL,
	`gender_id` TINYINT(3) UNSIGNED NOT NULL,
	`payment_id` TINYINT(3) UNSIGNED NOT NULL,
	`group_id` TINYINT(3) UNSIGNED NOT NULL,
	`documents` VARCHAR(500) NULL COLLATE 'utf8mb4_general_ci',
	`gender` VARCHAR(50) NULL COLLATE 'utf8mb4_general_ci',
	`payment` VARCHAR(50) NULL COLLATE 'utf8mb4_general_ci',
	`group_number` VARCHAR(20) NULL COLLATE 'utf8mb4_general_ci',
	`faculty` VARCHAR(50) NULL COLLATE 'utf8mb4_general_ci',
	`academic_year` TINYINT(3) UNSIGNED NULL,
	`fod` VARCHAR(50) NULL COLLATE 'utf8mb4_general_ci'
) ENGINE=MyISAM;

-- Дамп структуры для таблица studying_department.subjects
CREATE TABLE IF NOT EXISTS `subjects` (
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

-- Дамп данных таблицы studying_department.subjects: ~0 rows (приблизительно)
DELETE FROM `subjects`;
/*!40000 ALTER TABLE `subjects` DISABLE KEYS */;
INSERT INTO `subjects` (`id`, `name`) VALUES
	(1, 'ІАД'),
	(2, 'Кібернетика');
/*!40000 ALTER TABLE `subjects` ENABLE KEYS */;

-- Дамп структуры для таблица studying_department.subject_teacher
CREATE TABLE IF NOT EXISTS `subject_teacher` (
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `teacher_id` tinyint(3) unsigned NOT NULL,
  `subject_id` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `subject_fk` (`subject_id`),
  KEY `teacher_fk` (`teacher_id`),
  CONSTRAINT `subject_fk` FOREIGN KEY (`subject_id`) REFERENCES `subjects` (`id`),
  CONSTRAINT `teacher_fk` FOREIGN KEY (`teacher_id`) REFERENCES `teachers` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

-- Дамп данных таблицы studying_department.subject_teacher: ~0 rows (приблизительно)
DELETE FROM `subject_teacher`;
/*!40000 ALTER TABLE `subject_teacher` DISABLE KEYS */;
INSERT INTO `subject_teacher` (`id`, `teacher_id`, `subject_id`) VALUES
	(1, 1, 1),
	(2, 1, 2);
/*!40000 ALTER TABLE `subject_teacher` ENABLE KEYS */;

-- Дамп структуры для таблица studying_department.teachers
CREATE TABLE IF NOT EXISTS `teachers` (
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL DEFAULT '0',
  `admin_id` tinyint(3) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `admin_fk` (`admin_id`),
  CONSTRAINT `admin_fk` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

-- Дамп данных таблицы studying_department.teachers: ~0 rows (приблизительно)
DELETE FROM `teachers`;
/*!40000 ALTER TABLE `teachers` DISABLE KEYS */;
INSERT INTO `teachers` (`id`, `name`, `admin_id`) VALUES
	(1, 'Сергієнко Андрій Іванович', 6);
/*!40000 ALTER TABLE `teachers` ENABLE KEYS */;

-- Дамп структуры для представление studying_department.teachers_with_subjects
-- Создание временной таблицы для обработки ошибок зависимостей представлений
CREATE TABLE `teachers_with_subjects` (
	`id` TINYINT(3) UNSIGNED NOT NULL,
	`teacher_id` TINYINT(3) UNSIGNED NOT NULL,
	`teacher_name` VARCHAR(50) NOT NULL COLLATE 'utf8mb4_general_ci',
	`teacher_admin_id` TINYINT(3) UNSIGNED NULL,
	`subject_id` TINYINT(3) UNSIGNED NOT NULL,
	`subject_name` VARCHAR(50) NOT NULL COLLATE 'utf8mb4_general_ci'
) ENGINE=MyISAM;

-- Дамп структуры для представление studying_department.studentsview
-- Удаление временной таблицы и создание окончательной структуры представления
DROP TABLE IF EXISTS `studentsview`;
CREATE ALGORITHM=MERGE SQL SECURITY DEFINER VIEW `studentsview` AS SELECT s.id, s.last_name, s.first_name, s.surname, s.birthday, phone, home_address, next_payment,
	military_accounting_TF, academic_vacation_TF, expelled_TF, s.gender_id, s.payment_id, s.group_id, documents,
	(SELECT NAME FROM genders WHERE id = s.gender_id) AS gender,
	(SELECT NAME FROM payments WHERE id = s.payment_id) AS payment,
	(SELECT NAME FROM groups WHERE id = s.group_id) AS group_number,
	(SELECT NAME FROM faculties WHERE id = (SELECT faculty_id FROM groups WHERE id = s.group_id)) AS faculty,
	(SELECT academic_year FROM groups WHERE id = s.group_id) AS academic_year,
	(SELECT NAME FROM forms_of_education WHERE id = (SELECT form_of_education_id FROM groups WHERE id = s.group_id)) AS fod
FROM students AS s WITH CASCADED CHECK OPTION ;

-- Дамп структуры для представление studying_department.teachers_with_subjects
-- Удаление временной таблицы и создание окончательной структуры представления
DROP TABLE IF EXISTS `teachers_with_subjects`;
CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `teachers_with_subjects` AS SELECT st.id, st.teacher_id, t.name AS teacher_name, t.admin_id AS teacher_admin_id, st.subject_id, s.name AS subject_name
FROM subject_teacher AS st JOIN subjects AS s ON s.id = st.subject_id JOIN teachers AS t ON t.id = st.teacher_id ;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
